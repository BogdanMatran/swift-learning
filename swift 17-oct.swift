//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


enum Exercise: Int {
    case doi = 2
    case trei
    case patru
    case cinci
    
    init(value: Int) {
        switch value {
        case 2: self = .doi
        case 3: self = .trei
        case 4: self = .patru
        case 5: self = .cinci
        default:
            self = .doi
        }
    }
    
    init(value: String) {
        let intValue = Int(value)!
        self.init(value: intValue)
    }
}

class Person {
    var residence: Residence?
}

class Residence {
    var address: Address?
}

class Address {
    var street: String = "Ciurchi"
}

var bogdan = Person()
bogdan.residence = Residence()
bogdan.residence?.address = Address()
print(bogdan.residence!.address!.street)
if let street = bogdan.residence?.address?.street {
    print(street)
} else {
    print("Broken !")
}
if bogdan is Person {
    print("bogdan este o instanta de persoana ")
}

var array: [Any] = [Any]()
array.append(1)
array.append(0.1)
array.append("text")
array.append((4, 5, "test"))

for thing in array {
    switch thing{
    case let intValue as Int:
        print("Este de tipul INT \(intValue)")
    case let doubleValue as Double:
        print("Este double : \(doubleValue)")
    case is String:
        print ("Este string")
    case let (x, y, z) as (Int, Int, String):
        print("Tuple type: \(x) , \(y) and \(z)")
    default:
        break
    }
}
