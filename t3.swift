//: Playground - noun: a place where people can play

import UIKit

//var str = "Hello, playground"
//let http404error = (404,"Not found")
//print(http404error.0)
//var possibleString: String?
//
//if possibleString != nil
//{
//    print("exist")
//}
//else
//{
//    print("does not")
//}
////var exist: String = possibleString!
////print(exist)
//
//let x = -3
//let y = -x
//
//print(y)
//
//var test = possibleString ?? "b"
//print(test)
//let message = "\(x+y) osadaos "

class Animal {
    var name:String
    var age:Int
    init(name: String, age:Int) {
        self.name=name
        self.age=age
    }
    func sayHello(to person: String) -> Void {
        print("\(name) says miau to \(person)")
    }
    func addOneYear(to age: Int) -> Int {
        let newAge = age + 1
        return newAge
    }
}
let bubu = Animal(name: "Ion", age:4)
let bubuAge = bubu.addOneYear(to: bubu.age)
print("\(bubuAge)")

class Dog: Animal {
    var rasa: String
    override init(name: String, age:Int) {
        self.rasa = "Caine"
        super.init(name: name, age: age)
    }
}
let animal = Animal(name: "Labus", age:20)
animal.sayHello(to: "Dan")

class Doggo: Animal {
    var color: String
    
    override init(name: String, age: Int) {
        self.color = "Red"
        super.init(name: name, age: age)
    }
}
let rex = Doggo(name: "Rex", age: 11)
print("\(rex.name) has \(rex.age) years and \(rex.color) color")

struct dreptunghi {
    var inaltime: Int
    var latime: Int
    
    func arie() -> Int {
        return inaltime * latime
    }
}

let x = dreptunghi(inaltime: 20, latime: 10)
print("\(x.arie())")

enum Planet {
    case mercury, venus, earth, mars, jupiter, saturn, uranus, neptune
}

var planetToTravel = Planet.earth
//planetToTravel = .neptune //used to see how fallthrough works
planetToTravel = .mars
switch planetToTravel {
case .mercury:
    print("Going to Mercury")
case .venus:
    print("Going to Venus")
case .earth:
    print("Stay on Earth")
case .mars:
    print("Going on Mars")
case .jupiter:
    print("Going to Jupiter")
    fallthrough
default:
    print("Stay on Earth")
}




